library(shiny)
shinyUI(pageWithSidebar(
 headerPanel("Latent semantic indexing"),
 sidebarPanel(
 h4('Ustawienia:'),
 textInput("textInput", "Szukana fraza:", value = "protons"),
 radioButtons("dist", "Korpus:",
              c("tweeter" = 1,
                "arxiv" = 2),selected=2),
 radioButtons("szum", "Usuwanie szumu:",
              c("tak" = 1,
                "nie" = 2),selected=2),
 sliderInput('mu', 'współczynnik k:',value = 1000, min = 1, max = 1136, step = 1,),
 submitButton('Submit')
 ),
 mainPanel(
 h4('Rezultaty:'),
 tableOutput("view")
 )
))

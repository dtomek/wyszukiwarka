import urllib
import urllib2
import feedparser


base_url = 'http://export.arxiv.org/api/query?';

vocab = {}

search_query = 'all:electron' 
start = 0                     
max_results = 1200

query = 'search_query=%s&start=%i&max_results=%i' % (search_query,
                                                     start,
                                                     max_results)


feedparser._FeedParserMixin.namespaces['http://a9.com/-/spec/opensearch/1.1/'] = 'opensearch'
feedparser._FeedParserMixin.namespaces['http://arxiv.org/schemas/atom'] = 'arxiv'


response = urllib.urlopen(base_url+query).read()


feed = feedparser.parse(response)



# Run through each entry, and print out information
for entry in feed.entries:
   
    print 'Title:  %s' % entry.title
  
   
    for link in entry.links:
        if link.rel == 'alternate':
            pass	
        elif link.title == 'pdf':
	    vocab[entry.title.replace(" ", "").replace("/","").lower()[0:35]] = link.href

for title, address in vocab.iteritems():
	data = urllib2.urlopen(address)
	out = open('arxiv/'+title,'wb')
	out.write(data.read())
	out.close()

  
